package com.example.catlin.ooad;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
public class Play extends AppCompatActivity {

    ImageView imv;
    Integer dra[] = {R.drawable.p0, R.drawable.p1, R.drawable.p2,
            R.drawable.p3, R.drawable.p4, R.drawable.p3, R.drawable.p2,
            R.drawable.p1, R.drawable.p0};
    TextView tv;
    int i = 1;
    //private Handler handler = new Handler( );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        ImageView imageview = (ImageView)findViewById(R.id.imageView2);
        Intent intent = this.getIntent();
        String filename = intent.getStringExtra("filename");
        int imageResource = getResources().getIdentifier(filename,null,getPackageName());
        Drawable image = getResources().getDrawable(imageResource);
        imageview.setImageDrawable(image);
        imv = (ImageView) this.findViewById(R.id.imageView3);
        tv = (TextView) this.findViewById(R.id.textView1);
//postDelayed()執行序效果
        tv.postDelayed(new Runnable() {
            @Override
            public void run() {
                imv.setImageResource(dra[i % dra.length]);
                i++;
                //String s = Integer.toString(i);
                //tv.setText(s);

                tv.postDelayed(this, 300);
//1000=一秒鐘執行一次
            }
        }, 1000);
    }

    public void Back(View v){
        //tv.removeCallbacks()
        finish();
    }


}
