package com.example.catlin.ooad;

import android.preference.PreferenceActivity;

import java.io.Serializable;

/**
 * Created by CatLin on 2016/6/11.
 */
public class Status implements Serializable
{
    private int mood;
    private int health;
    //private String pic;
    public Status(int mood, int health)
    {
        this.mood = mood;
        this.health = health;
    }
    public String Display()
    {
        //System.out.printf("<Status> Mood:%d  Health:%d\n",this.mood,this.health);
        return "<Status> Mood:" + mood + "Health:" +health + "\n";
    }
    public void Judge()
    {

    }
    public int getMood()
    {
        return this.mood;
    }
    public int getHealth()
    {
        return this.health;
    }

}
