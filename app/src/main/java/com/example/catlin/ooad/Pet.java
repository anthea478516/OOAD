package com.example.catlin.ooad;

import java.io.Serializable;

public class Pet implements Serializable
{
    private int value;
    private int age;
    private String name;
    private Status status;
    private String source;

    public Pet(String name,int value,Status status,String source)
    {
        this.name = name;
        this.value = value;
        this.age = 0;
        this.status = status;
        this.source = source;
    }

    public String Display()
    {
        return "Name:" + name + "Age:" +age +"Value:" +value + "\n" + this.status.Display();
        //System.out.printf("Name:%s  Value:%d  Age:%d",this.name,this.value,this.age);
        //this.status.Display();
    }

    public String getName()
    {
        return this.name;
    }
    public int getValue()
    {
        return this.value;
    }
    public int getAge()
    {
        return this.age;
    }
    public Status getStatus()
    {
        return this.status;
    }
    public String getsrc(){return this.source; }

    private void Take_Walk()
    {

    }
    private void feed_them()
    {

    }
    private void shower()
    {

    }
    private void show_mood()
    {

    }

}
