package com.example.catlin.ooad;


/**
 * Created by pei on 2016/5/31.
 */
public class food extends item{

    int level;
    public food(String n, Integer p, String d,Integer l) {
        super(n, p, d);
        level=l;
    }

    public int getLevel()
    {
        return level;
    }

    @Override
    public void Display()
    {
        System.out.printf("Name:%s \nPrice:%d \nDescription:%s\nLevel:%d",name,price,description,level);
    }
}
