package com.example.catlin.ooad;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class Game extends AppCompatActivity {

    @BindView(R.id.pet_list)
    TextView pets;


    String showUrl = "http://140.122.184.227/~40147029S/user_pet.php";
    RequestQueue requestQueue;
    Player p;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);
        Intent intent = this.getIntent();
        p = (Player)intent.getSerializableExtra("player");

        requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, showUrl, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response.toString());
                        try {
                            JSONArray data = response.getJSONArray("data");
                            //這邊要和上面json的名稱一樣
                            //下邊是把全部資料都印出來
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jasondata = data.getJSONObject(i);
                                String user_name =jasondata.getString("userName");
                                if(user_name.equals(p.GetName())) {
                                    String name = jasondata.getString("petName");
                                    String value = jasondata.getString("value");
                                    String health = jasondata.getString("health");
                                    String mood = jasondata.getString("mood");
                                    String fileNmae = jasondata.getString("fileName");
                                    Status newstatus = new Status(Integer.valueOf(health).intValue(), Integer.valueOf(mood).intValue());
                                    Pet newpet = new Pet(name, Integer.valueOf(value).intValue(), newstatus,fileNmae);
                                    p.AddPet(newpet);
                                    //pets.append(name + ":\n價值:" + value + " 生命: " + health + " 心情: " + mood + " \n\n");
                                    int size = p.petSize();
                                }
                                //pets.append(Integer.toString(size)+"----- \n");

                                //db是textview
                            }

                            p.DisplayAllPets();

                            ListView L = (ListView)findViewById(R.id.petListView);
                            String[] petname;
                            Integer[] petid;

                            ArrayList<Pet> pets = p.getMypets();

                            petname = new String[ pets.size() ];
                            petid = new Integer[ pets.size() ];

                            for(int j=0;j<pets.size();j++) {
                                petname[j] = pets.get(j).getName();
                                petid[j] = getResources().getIdentifier( "@drawable/"+ pets.get(j).getsrc(), null, getPackageName() );
                            }

                            unCustomListAdapter adapter = new unCustomListAdapter(Game.this, petname, petid );
                            L.setAdapter(adapter);



                            //a.append("===\n");//把資料放到textview顯示出來

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.append(error.getMessage());
                    }
                });

        requestQueue.add(jsonObjectRequest);


    }

    @Override
    public void onResume() {
        super.onResume();

        ListView L = (ListView)findViewById(R.id.petListView);
        String[] petname;
        Integer[] petid;

        ArrayList<Pet> pets = p.getMypets();

        petname = new String[ pets.size() ];
        petid = new Integer[ pets.size() ];

        for(int j=0;j<pets.size();j++) {
            petname[j] = pets.get(j).getName();
            petid[j] = getResources().getIdentifier( "@drawable/"+ pets.get(j).getsrc(), null, getPackageName() );
        }

        unCustomListAdapter adapter = new unCustomListAdapter(this, petname, petid );
        L.setAdapter(adapter);
    }

    @OnClick(R.id.button2)
    public void petList(View view)
    {

        ArrayList<String> plist = new ArrayList<>();
        for(int i=0;i<p.petSize();i+=1)
        {
            plist.add(p.getMypets().get(i).getName());
        }


        new AlertDialog.Builder(Game.this)
                .setItems(plist.toArray(new String[plist.size()]), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Pet choosepet = p.getMypets().get(which);
                        String name = choosepet.getName();
                        //pets.append("you choose " + name + " \n");
                        Intent intent = new Intent(Game.this,Pet_interact.class);

                        //intent.putExtra ("pet_name",choosepet.getName());
                        Bundle mBundle = new Bundle();
                        mBundle.putSerializable("player",p);
                        mBundle.putInt("pet_id", which);
                        intent.putExtras(mBundle);
                        startActivity(intent);
                    }

                })
                .show();
    }
    @OnClick(R.id.shop)
    public void goshop()
    {
        Intent intent = new Intent(Game.this,PetShop.class);
        Bundle mBundle = new Bundle();
        mBundle.putSerializable("player",p);
        intent.putExtras(mBundle);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//如果切換的Activity沒有setResult();那麼Intent data=null、resultCode=0
        super.onActivityResult(requestCode, resultCode, data);

        p = (Player)data.getSerializableExtra("player");
        pets.setText(p.getMypets().get(p.getMypets().size()-1).getName());


    }


}
