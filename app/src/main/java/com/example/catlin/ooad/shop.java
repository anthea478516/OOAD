package com.example.catlin.ooad;


import java.util.ArrayList;

/**
 * Created by pei on 2016/5/30.
 */
public class shop {
    private  ArrayList<item> items;

    public shop() {
        items= new ArrayList<item>();
    }

    public  void Display_All() {
        for (int n = 0; n < items.size(); n++) {
            items.get(n).Display();
        }
    }
    public void AddItem(item new_item) {
        items.add(new_item);
    }
    public void DeleteItem(int index){
        items.remove(index);
    }
    public int size() {
        return items.size();
    }


}
