package com.example.catlin.ooad;

import java.io.Serializable;

import static java.lang.System.*;

/**
 * Created by pei on 2016/5/30.
 */
public class item implements Serializable {

    protected String name;
    protected int price;
    protected String description;

    public item(String n,int p,String d ){
        name=n;
        price=p;
        description=d;
    }

    public void Display() {
        System.out.printf("Name:%s \nPrice:%d \nDescription:%s\n",name,price,description);
    }
}

