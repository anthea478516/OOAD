package com.example.catlin.ooad;



import java.io.Serializable;
import java.util.ArrayList;

public class Player implements Serializable {
	private String account;
	private String password;
	private int money;
	private boolean LoginState;
	private int chosen_pet = -1;
	private ArrayList<Pet> mypets;
	private ArrayList<item> myitems;
	
	private void AddMoney(int m)
	{
		money+=m;
	}
	
	private void SubMoney(int m)
	{
		money-=m;
	}

	
	/*private boolean SoldPet(String id)
	{

	}
	
	private boolean Save()
	{
		
		
	}*/
	
	public Player(String account,String password,int money)
	{
		this.account=account;
		this.password=password;
		this.money=money;

		this.LoginState=true;
		this.mypets = new ArrayList<Pet>();
	}
	
	public boolean Login(String account,String password)
	{
		if(this.LoginState==false)
		{
			//System.out.println("Login ing...");
			if((this.account.equals(account)) & (this.password.equals(password)))
			{
				this.LoginState=true;
				System.out.println("Login successfully.");
				return true;
			}
		}
		System.out.println("Error to login.");
		return false;
		
	}
	
	public boolean Logout()
	{
		if(this.LoginState==true)
		{
			this.LoginState=false;
			System.out.println("Logout.");			
		}
		return true;
	}
	
	public boolean Buy(int price,Pet newpet)
	{
		if(this.money >= price)
		{
			/*
			if(AddPet(newpet)==true)
			{
				SubMoney(price);
				System.out.println("Buy.");
				return true;
			}
			System.out.println("Add pet error.");
			*/
		}
		else
		{
			System.out.println("No money to buy.");
		}
		return false;
	}
	
	public boolean Sold(int price,String id)
	{
		/*
		if(SoldPet(id)==true)
		{
			AddMoney(price);
			System.out.println("Sold.");
			return true;
		}
		*/
		System.out.println("You don't have the pet.");				
		return true;
	}
	
	public void DisplayMoney()
	{
		System.out.println(this.money);		
	}
	public int GetMoney()
	{
		return this.money;
	}
	
	public void Feed(String id)
	{
		//
	}
	public void Walk(String id)
	{
		//call pet walk function
	}
	public void AddPet(Pet newpet)
	{
		this.mypets.add(newpet);
	}

	public void DeletePet(int index)
	{
		this.mypets.remove(index);
	}

	public void DisplayAllPets()
	{
		for(int i=0;i<this.mypets.size();i++)
		{
			this.mypets.get(i).Display();
		}
	}
	public void setMoney(int m) { money = m; }
	public int petSize()
	{
		return this.mypets.size();
	}
	public int itemSize()
	{
		return this.myitems.size();
	}

	public ArrayList<Pet> getMypets()
	{
		return this.mypets;
	}
	public ArrayList<item> getMyitems()
	{
		return this.myitems;
	}
	public String GetName()
	{
		return account;
	}
	
	
}