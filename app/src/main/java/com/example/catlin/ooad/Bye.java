package com.example.catlin.ooad;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Bye extends AppCompatActivity {

    ImageView imv;
    int dra[] = {R.drawable.b0, R.drawable.b1, R.drawable.b2,
            R.drawable.b3};
    TextView tv;
    int i=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bye);
        ImageView imageview = (ImageView)findViewById(R.id.imageView);
        Intent intent = this.getIntent();
        String filename = intent.getStringExtra("filename");
        //filename = filename + "_b";
        int imageResource = getResources().getIdentifier(filename,null,getPackageName());
        Drawable image = getResources().getDrawable(imageResource);
        imageview.setImageDrawable(image);
        imv = (ImageView) this.findViewById(R.id.imageView1);
        tv = (TextView) this.findViewById(R.id.textView1);
//postDelayed()執行序效果
        tv.postDelayed(new Runnable() {
            @Override
            public void run() {
                imv.setImageResource(dra[i % dra.length]);
                i++;
                //String s = Integer.toString(i);
                //tv.setText(s);

                tv.postDelayed(this, 500);
//1000=一秒鐘執行一次
            }
        }, 500);
    }


    public void Back(View v){
        finish();
    }

}
