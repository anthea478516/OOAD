package com.example.catlin.ooad;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PetShop extends AppCompatActivity {

    @BindView(R.id.shopList)
    ListView list;

    String[] itemname = {
        "籠貓", "蘭貓", "喵^2", "楷弟貓",
        "辣腸", "不魯托", "鷹雄", "不丁狗","胖虎", "憤怒鳥"
    };
    String[] itemSrc = {
        "cat_01", "cat_02", "cat_03", "cat_04",
        "dog_01", "dog_02", "dog_03", "dog_04","gorilla", "bird"
    };

    Integer[] imgid={
            R.drawable.cat_01,
            R.drawable.cat_02,
            R.drawable.cat_03,
            R.drawable.cat_04,
            R.drawable.dog_01,
            R.drawable.dog_02,
            R.drawable.dog_03,
            R.drawable.dog_04,
            R.drawable.gorilla,
            R.drawable.bird
    };

    Integer[] price = {
            100,
            200,
            100,
            50,
            100,
            150,
            250,
            300,
            5,
            300
    };

    RequestQueue requestQueue;

    Player p;
    public Integer MyMoney, pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_shop);

        Intent intent = this.getIntent();
        p = (Player)intent.getSerializableExtra("player");

        TextView welcome = (TextView)findViewById(R.id.welcome_msg);
        welcome.append(p.GetName());

        TextView money = (TextView)findViewById(R.id.yourCash);
        money.setText("您現在有"+p.GetMoney()+"元可以花");
        MyMoney = p.GetMoney();

        CustomListAdapter adapter = new CustomListAdapter(this, itemname, imgid);
        list = (ListView)findViewById(R.id.shopList);
        list.setAdapter(adapter);

        //cash.setText("我有" + p.GetMoney() + "元");

        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                final String Slecteditem= itemname[position];
                pos = position;

                if(price[position] > p.GetMoney() ) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(PetShop.this);
                    dialog.setTitle("您的錢不夠喔...");
                    dialog.setMessage("非常遺憾...您的錢不足以購買" + Slecteditem + "\n"
                                      + Slecteditem + " = $ " + price[position]);

                    dialog.show();
                } else {

                    AlertDialog.Builder dialog = new AlertDialog.Builder(PetShop.this);
                    dialog.setTitle( "確定購買..?" );
                    dialog.setMessage("你確定要購買 " + Slecteditem + "嗎?" + "\n" +
                            "價格是 " + price[ position] + "元");

                    dialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    Toast.makeText( PetShop.this, "購買失敗", Toast.LENGTH_SHORT ).show();
                                }
                            }

                    );

                    dialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            // TODO Auto-generated method stub
                            final Integer myMoney2 = MyMoney - price[ pos ];
                            final String route = itemSrc[ pos ];
                            final String name = itemname[ pos ];

                            new AsyncTask<Void, Void, Void>() {
                                String response="";

                                @Override
                                protected Void doInBackground(Void... params) {
                                    try {
                                        URL url = new URL("http://140.122.184.227/~40147029S/confirmBuying.php");
                                        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                                        httpURLConnection.setRequestMethod("POST");
                                        httpURLConnection.setDoOutput(true);
                                        httpURLConnection.setDoInput(true);
                                        OutputStream outputStream = httpURLConnection.getOutputStream();
                                        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                                        String data = URLEncoder.encode("NewCash", "UTF-8") + "=" + URLEncoder.encode(myMoney2.toString(), "UTF-8") + "&" +
                                                      URLEncoder.encode("User", "UTF-8")    + "=" + URLEncoder.encode(p.GetName(), "UTF-8") + "&" +
                                                      URLEncoder.encode("petName", "UTF-8") + "=" + URLEncoder.encode(name, "UTF-8") + "&" +
                                                      URLEncoder.encode("route","UTF-8") + "=" + URLEncoder.encode( route, "UTF-8");
                                        bufferedWriter.write(data);
                                        bufferedWriter.flush();
                                        bufferedWriter.close();
                                        outputStream.close();
                                        InputStream inputStream = httpURLConnection.getInputStream();
                                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));

                                        String line = "";
                                        while ((line = bufferedReader.readLine()) != null) {
                                            response += line;
                                        }
                                        bufferedReader.close();
                                        inputStream.close();
                                        httpURLConnection.disconnect();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            }.execute();

                            p.setMoney( myMoney2 );
                            TextView money = (TextView)findViewById(R.id.yourCash);
                            money.setText("您現在有"+p.GetMoney()+"元可以花");
                            MyMoney = p.GetMoney();
                            p.AddPet(new Pet(itemname[pos],price[pos],new Status(200,300),itemSrc[pos]));


                            Toast.makeText(PetShop.this, "您已購買寵物",Toast.LENGTH_SHORT).show();
                        }
                    });

                    dialog.show();
                }


            }
        });


    }

    public void finish() {
        Intent intent = new Intent();
        Bundle mBundle = new Bundle();
        mBundle.putSerializable("player",p);
        intent.putExtras(mBundle);
        setResult(0,intent);//將intent以及ResultCode回傳
        super.finish();
    }
    
}
