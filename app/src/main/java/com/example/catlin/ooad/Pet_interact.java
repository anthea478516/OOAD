package com.example.catlin.ooad;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Pet_interact extends AppCompatActivity {

    @BindView(R.id.pet)
    TextView pet;
    //Intent intent = this.getIntent();
    //Bundle bundle = getIntent().getExtras();
    Player p ;
    int pet_id;
    Pet pp;
    String url = "@drawable/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_interact);
        ButterKnife.bind(this);
        TextView t = (TextView)findViewById(R.id.money);

        Intent intent = this.getIntent();
        Bundle bundle = getIntent().getExtras();
        p = (Player)intent.getSerializableExtra("player");
        pet_id =bundle.getInt("pet_id");
        t.setText("Money:" +  p.GetMoney() + "");
        //pet.append(p.GetName()+ "\n");
        //pet.append(p.getMypets().get(pet_id).Display());

        pp=p.getMypets().get(pet_id);
        ImageView imageview = (ImageView)findViewById(R.id.petimage);
        url =url + pp.getsrc();
        int imageResource = getResources().getIdentifier(url,null,getPackageName());
        Drawable image = getResources().getDrawable(imageResource);
        imageview.setImageDrawable(image);


        pet.append(pp.getName() + ":\n價值:" + pp.getValue() + " 生命: " + pp.getStatus().getHealth() + " 心情: " + pp.getStatus().getMood() + " \n\n");

        //String name = intent.getStringExtra("player_name");
        //String pet_name = intent.getStringExtra("pet_name");
        //pet.append(name);
        //pet.append(p);



    }
    @OnClick(R.id.feed)
    public void gofeed()
    {
        Intent intent = new Intent(Pet_interact.this,Feed.class);
        //intent.putExtra("filename",url);
        Bundle mBundle = new Bundle();
        mBundle.putString("filename",url);
        intent.putExtras(mBundle);
        startActivity(intent);
    }
    @OnClick(R.id.play)
    public void goplay()
    {
        Intent intent = new Intent(Pet_interact.this,Play.class);
        Bundle mBundle = new Bundle();
        mBundle.putString("filename",url);
        intent.putExtras(mBundle);
        startActivity(intent);
    }
    @OnClick(R.id.sell)
    public void gosell()
    {
        final Integer new_cash = p.GetMoney() + p.getMypets().get(pet_id).getValue();
        new AsyncTask<Void, Void, String>() {
            String response = "";

            @Override
            protected String doInBackground(Void... params) {
                try {
                    URL url = new URL("http://140.122.184.227/~40147029S/sold.php");
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String data = URLEncoder.encode("User", "UTF-8") + "=" + URLEncoder.encode(p.GetName(), "UTF-8") + "&" +
                            URLEncoder.encode("NewCash", "UTF-8") + "=" + URLEncoder.encode(new_cash.toString(), "UTF-8") +
                            "&" + URLEncoder.encode("pet", "UTF-8") + "=" + URLEncoder.encode(pp.getName(), "UTF-8");
                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));

                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        response += line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }
            @Override
            protected void onPostExecute(String response) {
                Intent intent = new Intent(Pet_interact.this,Bye.class);
                Bundle mBundle = new Bundle();
                mBundle.putString("filename",url);
                intent.putExtras(mBundle);
                startActivity(intent);
            }
        }.execute();

    }
}
